#include <limit.h>

/*
    TODO:
        Clean up limit_compare.
        My limit is 80 columns. Make it so.
*/

limit_cell limit_char_size   = sizeof(limit_char);
limit_cell limit_cell_size   = sizeof(limit_cell);
limit_cell limit_buffer_size = limit_buffer_length;

limit_bool limit_buffer_new(limit_buffer *buffer,
                            limit_cell    cursor) {
    limit_cell iterator;
    if(buffer == limit_null) {
        return limit_false;
    }
    for(iterator = 0;
        iterator < limit_buffer_size;
        iterator++) {
        buffer->data[iterator] = 0;
    }
    buffer->cursor = cursor;
    return limit_true;
}

limit_bool limit_delimiter(limit_buffer *delimiters,
                           limit_char    character) {
    limit_cell iterator;
    if(delimiters == limit_null) {
        return limit_false;
    }
    for(iterator = 0;
        iterator < delimiters->cursor &&
        iterator < limit_buffer_size;
        iterator++) {
        if(character == delimiters->data[iterator]) {
            return limit_true;
        }
    }
    return limit_false;
}

limit_bool limit_skip(limit_buffer *buffer,
                      limit_buffer *delimiters,
                      limit_bool   delimiter) {
    limit_cell iterator;
    if(buffer     == limit_null ||
       delimiters == limit_null) {
        return limit_false;
    }
    for(iterator = buffer->cursor;
        iterator < limit_buffer_size;
        iterator++) {
        if(limit_delimiter(delimiters, buffer->data[iterator]) != delimiter) {
            break;
        }
        buffer->cursor++;
    }
    return limit_true;
}

limit_cell limit_length(limit_buffer *buffer,
                        limit_buffer *delimiters,
                        limit_bool    delimiter) {
    limit_cell iterator;
    if(buffer     == limit_null ||
       delimiters == limit_null) {
        return 0;
    }
    for(iterator = buffer->cursor;
        iterator < limit_buffer_length;
        iterator++) {
        if(limit_delimiter(delimiters, buffer->data[iterator]) != delimiter) {
            break;
        }
    }
    return (iterator - buffer->cursor);
}

limit_bool limit_compare(limit_buffer *a,
                         limit_buffer *b,
                         limit_buffer *delimiters,
                         limit_bool    delimiter) {
    limit_cell iterator;
    limit_cell length;
    limit_bool result;
    if(a          == limit_null ||
       b          == limit_null ||
       delimiters == limit_null) {
        return limit_false;
    }
    length = limit_length(a, delimiters, delimiter);
    if(length != limit_length(b, delimiters, delimiter)) {
        return limit_false;
    }
    result = limit_false;
    for(iterator             = 0;
        a->cursor + iterator < limit_buffer_size &&
        b->cursor + iterator < limit_buffer_size;
        iterator++) {
        if(limit_delimiter(delimiters, a->data[a->cursor + iterator]) != delimiter &&
           limit_delimiter(delimiters, b->data[b->cursor + iterator]) != delimiter) {
            break;
        }
        if(a->data[a->cursor + iterator] != b->data[b->cursor + iterator]) {
            return limit_false;
        }
        else {
            result = limit_true;
        }
    }
    return result;
}
