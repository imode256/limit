#if !defined(limit_char_type)
    #if !defined(limit_defaults)
        #error "No character type defined."
    #else
        #define limit_char_type char
    #endif
#endif
#if !defined(limit_cell_type)
    #if !defined(limit_defaults)
        #error "No cell type defined."
    #else
        #define limit_cell_type unsigned int
    #endif
#endif
#if !defined(limit_buffer_length)
    #if !defined(limit_defaults)
        #error "No buffer size defined."
    #else
        #define limit_buffer_length 256
    #endif
#endif
#if !defined(limit_null)
    #if !defined(limit_defaults)
        #error "No null value defined."
    #else
        #define limit_null ((void*)0)
    #endif
#endif
typedef limit_char_type limit_char;
typedef limit_cell_type limit_cell;
typedef unsigned char   limit_bool;
enum {
    limit_false = 0,
    limit_true  = !limit_false
};

typedef struct limit_buffer limit_buffer;

struct limit_buffer {
    limit_char data[limit_buffer_length];
    limit_cell cursor;
};

extern limit_cell limit_char_size;
extern limit_cell limit_cell_size;
extern limit_cell limit_buffer_size;

limit_bool limit_buffer_new(limit_buffer *buffer,
                            limit_cell    cursor);
limit_bool limit_delimiter (limit_buffer *delimiters,
                            limit_char    character);
limit_bool limit_skip      (limit_buffer *buffer,
                            limit_buffer *delimiters,
                            limit_bool    delimiter);
limit_cell limit_length    (limit_buffer *buffer,
                            limit_buffer *delimiters,
                            limit_bool    delimiter);
limit_bool limit_compare   (limit_buffer *a,
                            limit_buffer *b,
                            limit_buffer *delimiters,
                            limit_bool    delimiter);
